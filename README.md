# Liste des libs validées 
https://gitlab.com/clemcasa/awesome_libs/wikis/home

# Règles à respecter
## Facilité d'intégration
La bibliothèque doit être facilement intégrable flexible et doit pouvoir donner un accès à tous les éléments natifs.

## Bibliothèque à jour
La bibliothèque doit-être à jour et soutenue sur les dernières versions des OS. Swift3 pour les lib en Swift

## Testé et approuvé par LinkValue
La bibliothèque ne doit-être ajoutée que lorsqu'elle a été utilisée en condition réelle d'un projet d'une taille respectable et non pas que dans le sample.
Cependant la bibliothèque doit être suffisamment générique pour rentrer dans plusieurs projets

## Pas de warning à la compilation
La biliothèque ne doit pas contenir de lignes de code dépréciées, de warning de compilation de type, etc....
